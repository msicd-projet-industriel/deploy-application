>_Date:_ Lundi 21 MARS 2023.
>
> **Mastère Spécialisé - Infrastructure Cloud et  DevOps 2022-23**
> 
> GROUPE 3
>


# Déploiement de  l'application méteo-icd

# Présentation

Ce dépôt permet de déployer la pile d'application meteo dans le cluster kubernetes  avec la chaîne de déploiement continue Gitlab de ce projet. 

Les playbook déploient essentiellement les conteneurs kubernetes de l'application à partir des définitions des manifests et à l'aide du module `kubernetes.core.k8s`.

 Les principaux playbook sont:

- [x]   `environment.yml ` qui installe les dépendances python et les modules nécessaires à ansible pour faire les déploiements de paquetage helm ou  de manifests kubernetes; il installe aussi l'outil `kubectl` pour l'utilisateur ansible sur le noeud défini à travers la variable `rke_node`. 
- [x]  ` deploy_secrets.yml` pour les secrets;
- [x]  ` deploy_volumes.yml` pour les volumes persistents;
- [x]  ` deploy_frontend.yml` pour le les pod de l'application météo;
- [x]  ` deploy_ingress.yml`  pour les règles ingress basées sur le nom d'hôte.


# Utilisation du dépôt

Le fichier de chaîne de déploiement continue `.gitlab-ci.yml` dans ce dépôt construit une image d'exécution de job ansible  à l'aide du Dockerfile à la racine du projet. Il exécute d'abord les jobs d'un premier stage `prepare` pour installer toutes les dépendances de librairies python, les rôles  et les collections qui sont indispensables pour les jobs du stage de déploiement `deploy`. 

Le déploiement est assuré par le stage `deploy`.  


## Pré-requis

Pour personnaliser le déploiement , il faut disposer dans le projet des fichiers de configuration ansible et SSH avec l'inventaire des noeuds. Ces derniers sont téléchargés automatiquement du registre de paquetage  du projet de déploiement de l'infrastructure Openstack. 

La seule information nécessaire est la clé privée autorisée sur les instances dans le fichier d'inventaire. Il s'agit des variables Gitlab de type fichier : la paire `OPENSTACK_PRIV_KEY` et  `OPENSTACK_PUB_KEY`. 

 En cas d'expiration ou de révocation des jetons de déploiement il sera nécessaire de les mettre à jour à travers les variables  `CI_ANSIBLE_TOKEN_USERNAME`, `CI_ANSIBLE_TOKEN` et `CI_GITLAB_REGISTRY_TOKEN` toutes définies au niveau du groupe de projets.


## Déploiement de la pile d'application meteo

- [x] Enregistrer un runner
 
Le runner doit pouvoir faire du Docker in Docker (DinD) avec les tags iac, master.

```bash
sudo gitlab-runner register --url https://gitlab.imt-atlantique.fr \
       --name docker-executor-for-meteoapp \
       --tag-list iac,master --executor docker
```

- [x] Modifier les variables de déploiement dans les fichiers suivants (***optionnel***):

 `group_vars/masters.yml` :  pour définir par exemple `meteo_app_hostname` pour le nom de domaine du endpoint d'acces a l'application depuis l'exterieur du cluster (un proxy inverse ou un loadbalancer).

 `group_vars/all.yml`: pour définir de nouveaux paramètres de proxy.

- [x] Modifier les variables Gitlab liées au nouvel environnement selon les pré-requis, notamment `OPENSTACK_PRIV_KEY`,  et `OPENSTACK_PUB_KEY` et au besoin celles des jetons `CI_ANSIBLE_TOKEN_USERNAME`, `CI_ANSIBLE_TOKEN` et `CI_GITLAB_REGISTRY_TOKEN` en cas d'expiration ou de révocation. Les autres variables importantes sont contenues dans le fichier de variables Gitlab `METEO_PROD_VARS` notamment la version de l'image `METEO_IMAGE_VERSION`.

- [x] Déclencher manuellement un nouveau cycle d'exécution du pipeline GitLab (CI/CD > Pipelines > Bouton Run pipeline) ou automatiquement par iteration avec les commit en fixant un `tag` satifaisant la regle `*-release`. Le dernier `tag` le plus sur a la redaction de ce README etant `1.10-release`.

```sh
git add .
git commit -u origin master
git push -u origin master
git tag -a 1.10-release -m "Update" && git push -u origin 1.10-release
```

## Test
Utiliser la clé SSH autosisée pour se connecter au noeud  `control1`  qui est celui par défaut sur lequel l'outil `kubectl` est installé et configuré pour le cluster (ou celui défini par la variable `rke_node` ).

Copier le fichier artefact ssh.cfg dans le répertoire local ainsi que la clé privée authorisée `OPENSTACK_PRIV_KEY`

 Vérifier ensuite que la pile est bien déployée:
```sh
$ ssh -F ssh.cfg 172.16.30.201
$ kubectl get all -n meteo
``` 
`meteo` est le namespace défini par défaut.

Depuis un poste client  externe à Openstack, configurer la résolution de noms dans /etc/hosts en ajoutant la ligne :
```sh
adresse_FIP api.meteo.dev app.meteo.dev
```
`adresse_FIP` désigne une des adresses IP flottantes membres du groupe [endpoint] dans le fichier artefact inventory; c'est simplement un des noeuds workers. 

Enfin, visiter l'URL http://app.meteo.dev/ pour utiliser l'application meteo.

Utiliser le dashboard de traefik pour apprécier le statut des règles ingress. Pour cela ouvrir un terminal sur le noeud `control1` et faire un port-forward:

```sh
$ kubectl -n traefik port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefik" --output=name -A | head -1) 9000:9000 --address 0.0.0.0 
```
A l'aide du navigateur rendez-vous à l'url: http://adresse_FIP:9000/dashboard/ 

>NB:
> le slash à la fin 
> et adresse_FIP est l'adresse IP flottante du noeud défini par la variable `rke_node`, par défaut `control1`.

Les deux règles suivantes doivent figurer dans `HTTP Routes`:
```sh
Host(app.meteo.dev) 
```
